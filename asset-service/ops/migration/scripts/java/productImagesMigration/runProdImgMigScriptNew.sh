#!/bin/bash
#INSTRUCTIONS : Before running this script, depending upon the environment in which you are running you will have to change following parameters
# - dbusername - dbpassword - host - dburl - cmsserviceUrl

currentDir=$(pwd);
fileWithStyleIds=$currentDir"/stylesToMigrate-"$(date +"%Y-%m-%d")"-"$(date +"%s")".txt"

logfile=$currentDir"/migrationLog-"$(date +"%Y-%m-%d")"-"$(date +"%s")".log"

echo $logfile

#tables
tableStylesFromSolr="myntra.Products_in_stock_temp";
tableProductImageMigration="myntra.productImageMigration";

#variable required
#dbusername="root" ; dbpassword="paroksh" ; host="localhost" ; dburl="jdbc:mysql://localhost:3306/myntra"
#cmsserviceUrl="http://localhost:8080/myntra-cms-service"

#prod urls :
dbusername="MyntraCMSUser";dbpassword="2x?pRSz3n";host="pmdb1.myntra.com";dburl="jdbc:mysql://pmdb1.myntra.com:3306/myntra";cmsserviceUrl="http://localhost:7070/myntra-cms-service"
#qa urls :
#dbusername="MyntraCMSUser" ; dbpassword="iFytWRThr6uBs" ; host="qa1db1.myntra.com" ; dburl="jdbc:mysql://qa1db1.myntra.com:3306/myntra" ; cmsserviceUrl="http://localhost:7070/myntra-cms-service"
#preprod urls :
#preprod urls : 
#dbusername="MyntraCMSUser" ; dbpassword="iFytWRThr6uBs" ; host="delta7mdb.myntra.com" ; dburl="jdbc:mysql://delta7mdb.myntra.com:3306/myntra" ; cmsserviceUrl="http://localhost:7070/myntra-cms-service"

query="select solrTable.product_id from $tableStylesFromSolr solrTable left outer join $tableProductImageMigration pim on pim.product_id = solrTable.product_id where (pim.migrated is null or pim.migrated=0) order by solrTable.product_id desc"
echo "Query is : "$query >> $logfile

#clearing file
printf "" > $fileWithStyleIds

#reading output of query line by line
styleIdsCSV="";
postBody="[";

queryOutput=$(mysql myntra -h$host -u$dbusername -p$dbpassword -se "$query")
for line in $queryOutput
do
	styleIdsCSV=$styleIdsCSV$line",";
        postBody=$postBody"{ \"id\" : $line, \"action\" : \"migrate\" },";
done

#removing last character (comma) from style ids csv
styleIdsCSV=${styleIdsCSV%?}
postBody=${postBody%?}

postBody=$postBody"]";

#writing style ids csv to a file
printf $styleIdsCSV >> $fileWithStyleIds

echo "Body of post request is : $postBody" >> $logfile;

#Making call to migration API
response=$(curl -s -X POST "$cmsserviceUrl/catalog/migration/" --data "$postBody" --header "Authorization: Basic YXBpYWRtaW46bTFudHJhUjBja2V0MTMhIw==" -H "Content-Type: application/json" -H "Accept: application/json");
echo "response is : $response" >> $logfile;
status=$(exec echo $response | jq '.status.statusType');
echo "Status is : $status" >> $logfile;

echo "Bash script successfully executed" >> $logfile
