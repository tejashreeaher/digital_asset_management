#Steps to follow

1) Run the migrationClasspath.sh file.
It takes as input location of the folder containing all the jar files required to compile ProductImagesMigrationScript file
It also takes as input location of the file where classpath needs to be generated

2) Compile and run ProductImagesMigrationScript file using the classpath generated in the previos step

#Format of command to run from command line : 

#For migrationClasspath shell script

terminal> sh migrationClasspath.sh
>Give as input location of the folder containing all the jars
<here you need to provide the path of the folder containing jars> (eg : /home/paroksh/myntra/installed_sft/apache-tomcat-7.0.6-1/webapps/myntra-cms-service/WEB-INF/lib)
>Give as input the location of the output file to write classpath to
<here you need to give location of the output file where classpath will be written> (eg : /tmp/mcp )
>Successfull written classpath to location /tmp/mcp

For running of script, either you can specify range of styleIds for which migration is to be done or you can specify a .csv file containing comma separated list of styleIds which are to be migrated
Lets assume location where classpath is written is /tmp/mcp

#For compiling
javac -cp `cat /tmp/mcp` ProductImagesMigrationScript.java

#For running
java -cp `cat /tmp/mcp` ProductImagesMigrationScript <dbusername> <dbpassword> <dburl> <cmsserviceUrl> <styleIdFrom> <styleIdTo>

or 
java -cp `cat /tmp/mcp` ProductImagesMigrationScript <dbusername> <dbpassword> <dburl> <cmsserviceUrl> <fileName.csv>

#where 
#dbusername    : username for connecting to database
#dbpassword    : password for connecting to database
#dburl         : database url
#cmsserviceUrl : url of cmsservice  (eg : http://localhost:8080/myntra-cms-service)
#styleIdFrom   : style id from which to start migration (including)
#styleIdTo     : style id till which migration to be done (including)
#fileName.csv  : file containing comma separated list of styles which are to be migrated

Eg of db credentials :
root paroksh jdbc:mysql://localhost:3306/myntra http://localhost:8080/myntra-cms-service 1531 1531

Examples:
java -cp .:`cat /tmp/mcp` ProductImagesMigrationScript root paroksh jdbc:mysql://localhost:3306/myntra http://localhost:8080/myntra-cms-service 1531 1531

if classpath requried is added to the variable $CLASSPATH, then directly command can be run as follows :
java ProductImagesMigrationScript root paroksh jdbc:mysql://localhost:3306/myntra http://localhost:8080/myntra-cms-service 1531 1531
