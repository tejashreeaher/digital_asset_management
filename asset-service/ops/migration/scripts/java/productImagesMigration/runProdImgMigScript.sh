#!/bin/bash
#INSTRUCTIONS : Before running this script, depending upon the environment in which you are running you will have to change following parameters
# 1) jarLocation 2) dbusername 3) dbpassword 4) host 5) dburl 6) cmsserviceUrl

#location where all required jars are located. It is the path of lib folder of deployed cmsservice
jarLocation="/myntra/myntra-cms-service/releases/current/webapps/myntra-cms-service/WEB-INF/lib";
#jarLocation="/Users/paroksh.saxena/softwares/apache-tomcat-7.0.54/webapps/myntra-cms-service/WEB-INF/lib";

currentDir=$(pwd);
jarFile="/tmp/parJar.txt"
fileWithStyleIds=$currentDir"/stylesToMigrate-"$(date +"%Y-%m-%d")"-"$(date +"%s")".txt"

logfile=$currentDir"/migrationLog-"$(date +"%Y-%m-%d")"-"$(date +"%s")".log"

echo $logfile

#tables
tableStylesFromSolr="myntra.Products_in_stock_temp";
tableProductImageMigration="myntra.productImageMigration";

mypath=".";
for file in $jarLocation/*.jar
do 
mypath=$mypath:$file
done
echo $mypath > $jarFile
echo "Successfull written classpath to location " $jarFile >> $logfile


#variable required
#dbusername="root" ; dbpassword="paroksh" ; host="localhost" ; dburl="jdbc:mysql://localhost:3306/myntra"
#cmsserviceUrl="http://localhost:8080/myntra-cms-service"

#prod urls :
dbusername="MyntraCMSUser";dbpassword="2x?pRSz3n";host="pmdb1.myntra.com";dburl="jdbc:mysql://pmdb1.myntra.com:3306/myntra";cmsserviceUrl="http://localhost:7070/myntra-cms-service"
#qa urls :
#dbusername="MyntraCMSUser" ; dbpassword="iFytWRThr6uBs" ; host="qa1db1.myntra.com" ; dburl="jdbc:mysql://qa1db1.myntra.com:3306/myntra" ; cmsserviceUrl="http://localhost:7070/myntra-cms-service"
#preprod urls :
#preprod urls : 
#dbusername="MyntraCMSUser" ; dbpassword="iFytWRThr6uBs" ; host="delta7mdb.myntra.com" ; dburl="jdbc:mysql://delta7mdb.myntra.com:3306/myntra" ; cmsserviceUrl="http://localhost:7070/myntra-cms-service"

#query="select id from mk_product_style ps left outer join productImageMigration pim on ps.id = pim.product_id where ps.styleType = 'P' and (pim.migrated is null or pim.migrated = 0) order by id desc limit 4"
query="select solrTable.product_id from $tableStylesFromSolr solrTable left outer join $tableProductImageMigration pim on pim.product_id = solrTable.product_id where (pim.migrated is null or pim.migrated=0) order by solrTable.product_id desc"
echo "Query is : "$query >> $logfile

#clearing file
printf "" > $fileWithStyleIds

#reading output of query line by line
styleIdsCSV="";

queryOutput=$(mysql myntra -h$host -u$dbusername -p$dbpassword -se "$query")
for line in $queryOutput
do
	styleIdsCSV=$styleIdsCSV$line","
done

#echo $queryOutput;

#removing last character (comma) from style ids csv
styleIdsCSV=${styleIdsCSV%?}

#writing style ids csv to a file
printf $styleIdsCSV >> $fileWithStyleIds


echo "Executing command" >> $logfile
echo "javac -cp $jarFile ProductImagesMigrationScript.java"

echo "java -cp  $jarFile ProductImagesMigrationScript $dbusername $dbpassword $dburl $cmsserviceUrl $fileWithStyleIds"
javac -cp `cat $jarFile` ProductImagesMigrationScript.java >> $logfile 2>&1

java -cp `cat $jarFile` ProductImagesMigrationScript $dbusername $dbpassword $dburl $cmsserviceUrl $fileWithStyleIds >> $logfile 2>&1

echo "Bash script successfully executed" >> $logfile
