#!/bin/bash
#This file reads styles having count_options_availbale > 1 from solr and put those styles in a table
#get current directory
dir=$(pwd);
echo "Todays date is : "$(date +"%Y-%m-%d");

csvfile=$dir"/stylesFromSolr-"$(date +"%Y-%m-%d")"-"$(date +"%s")".csv"

#Below parameters needs to be environmentalized
#Connecting to database and inserting this csv into a table
#dbusername="root";dbpassword="paroksh";dbhost="localhost";
#Solr url
#solrURL="http://54.251.99.19:8984/";

#prod urls :
dbusername="MyntraCMSUser";dbpassword="2x?pRSz3n";dbhost="pmdb1.myntra.com";solrURL="http://solrfarm.myntra.com:8984/";
#qa urls : 
#dbusername="MyntraCMSUser" ; dbpassword="iFytWRThr6uBs" ; dbhost="qa1db1.myntra.com" ; solrURL="http://10.148.1.3:8984/"
#preprod urls : 
#dbusername="MyntraCMSUser" ; dbpassword="iFytWRThr6uBs" ; dbhost="delta7mdb.myntra.com" ; solrURL=

table="myntra.Products_in_stock_temp";


#Delete table
echo "Connecting to db and deleting table migrationTemp ...";
query="truncate $table";
mysql myntra -h$dbhost -u$dbusername -p$dbpassword -se "$query"

echo "Deleted all entries from migrationTemp table";


#Num of styles fetched from solr at a time
start=0;
rows=1000;

#Get solr result
url=$solrURL"solr/sprod/select?q=count_options_availbale%3A[1+TO+*]&start="$start"&rows="$rows"&fl=styleid&wt=json&indent=true"
solrRes=$(curl -s --globoff $url);
numFound=$(exec echo $solrRes | jq ".response.numFound");
echo $numFound

#set number of elements found in current solr query result

if [ $(($start+$rows)) -gt $numFound ]
then
	numofElements=$(($numFound-$start));
else
	numofElements=$rows;
fi
	
#keep iterating till total values processed is equal to total num found
styleCSV="";

while [ $start -lt $numFound ]
do
	echo "start : $start rows : $rows numFound : $numFound numofElements : $numofElements"
	output=$(exec echo $solrRes | jq '.response.docs');	

	#looping over result and extracting styleids csv in a variable
	
	for ((i=0;i<$numofElements;i++))
	do
		eachStyle=$(exec echo $output | jq ".[$i] | .styleid");
		styleCSV=$styleCSV$eachStyle",";
	done
	
	#after iterating on current result, increase start by numofElements found
	start=$(($start+$numofElements));
	
	url=$solrURL"solr/sprod/select?q=count_options_availbale%3A[1+TO+*]&start="$start"&rows="$rows"&fl=styleid&wt=json&indent=true"
	solrRes=$(curl -s --globoff $url);
	
	if [ $(($start+$rows)) -gt $numFound ]
	then	
		numofElements=$(($numFound-$start));
	else
		numofElements=$rows;
	fi

done

#removing last character (comma) form style csv
styleCSV=${styleCSV%?}

#writing style csv to a file
printf $styleCSV >> $csvfile;



query="load data local infile '$csvfile' into table $table lines terminated by ','"
#query="select count(*) from productImageMigration";
mysql myntra -h$dbhost -u$dbusername -p$dbpassword -se "$query"

				

