//package com.myntra.erp.cms.migration;

import com.myntra.commons.auth.ContextInfo;
import com.myntra.commons.exception.WebClientException;
import com.myntra.erp.cms.client.ContentClient;
import com.myntra.erp.cms.client.CatalogClient;
import com.myntra.erp.cms.client.entry.AssetEntry;
import com.myntra.erp.cms.client.entry.AssetResolution;
import com.myntra.erp.cms.client.entry.CollectionMemberEntry;
import com.myntra.erp.cms.client.entry.ContentEntry;
import com.myntra.erp.cms.client.entry.ProductAlbumEntry;
import com.myntra.erp.cms.client.entry.ProductContentEntry;
import com.myntra.erp.cms.client.entry.ProductImageEntry;
import com.myntra.erp.cms.client.response.ContentResponse;
import com.myntra.erp.cms.client.response.ProductResponse;
import com.myntra.erp.cms.enums.EntityAttributeEnum;
import com.myntra.erp.cms.enums.EntityTypeEnum;
import com.myntra.erp.cms.enums.ViewType;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author paroksh
 */
public class ProductImagesMigrationScript { //extends HttpServlet {

    //for jdbc connection. They are taken from command line input
    private static String dbURL;// = "jdbc:mysql://localhost:3306/myntra";
    private static String username;// = "root";
    private static String password;// = "paroksh";
    private static Connection con = null;
    //name corresponds to column name in db
    private static final String DEFAULT = "default_image";
    private static final String LEFT = "left_image";
    private static final String RIGHT = "right_image";
    private static final String FRONT = "front_image";
    private static final String BACK = "back_image";
    private static final String BOTTOM = "bottom_image";
    private static final String TOP = "top_image";
    private static final String SEARCH = "search_image";
    private static final String STYLE_ID = "style_id";
    private static final String PRODUCT_ID = "product_id";
    private static final String MIGRATED_COLUMN = "migrated";
    private static final String IS_EXCLUDED_COLUMN = "isExcluded";
    private static Long styleIdFrom;
    private static Long styleIdTo;
    private static String styleIdsCSV;
    //private static final String SIZE_CHART = "size_representation_image_url";
    //text_color_image, search_zoom_image
    //script related values
    private static final String DOMAIN_S3 = "http://myntra.myntassets.com/";
    private static final String DOMAIN_CL = "http://assets.myntassets.com/";
    private static final String PRODUCT_IMAGE = EntityTypeEnum.PRODUCT_IMAGE.toString();
    private static final String PRODUCT_ALBUM_TYPE = EntityTypeEnum.PRODUCT_ALBUM.toString();
    private static final String PRODUCT = EntityTypeEnum.PRODUCT.toString();
    private static ContextInfo info;
    //service url is taken as input from command line
    private static String serviceURL;
    //map containing column_name of images to their view Type enum value mapping
    private static Map<String, String> ImageTypeToViewTypeMap;
    //map containing inverse mapping of  column_name of images to their view Type enum value
    private static Map<String, String> ViewTypeToImageTypeMap;

    //For using threads
    private static Integer numofThreads;
    private static ExecutorService taskExecutor;
    private static ExecutorCompletionService completionService;
    private static Integer productImageCreationTimeOut;

    //Variable for checking mismatch in num of images in cms and portal. This variable is per image level so it needs to be reset before starting processing an image
    private static Boolean imageNumMismatch = Boolean.FALSE;
    /**
     * Fills contextInfo variable with dummy values
     */
    private static void initialize() {
        //filling required value in context info to be passed to client
        info = new ContextInfo();
        info.setPassword("temp");
        info.setName("system");
        info.setLoginId("system");

        //filling map containing column_name of images to their view Type enum value mapping
        ImageTypeToViewTypeMap = new HashMap<String, String>();
        ViewTypeToImageTypeMap = new HashMap<String, String>();
        fillImageToViewMap();
    }

    private static void initializeThreads() {
        numofThreads = 8; //number of images
        taskExecutor = Executors.newFixedThreadPool(numofThreads);
        completionService = new ExecutorCompletionService(taskExecutor);
        productImageCreationTimeOut = 5;//in minutes
    }

    private static void finishThreads() {
        if (!taskExecutor.isShutdown()) {
            taskExecutor.shutdownNow();
        }
    }

    /**
     * Creates a mapping from image column name to its view type name and also
     * other way round
     *
     * @param imageToViewMap
     * @return
     */
    private static void fillImageToViewMap() {
        if (ImageTypeToViewTypeMap == null) {
            ImageTypeToViewTypeMap = new HashMap<String, String>();
        } else {
            ImageTypeToViewTypeMap.clear();
        }
        if (ViewTypeToImageTypeMap == null) {
            ViewTypeToImageTypeMap = new HashMap<String, String>();
        } else {
            ViewTypeToImageTypeMap.clear();
        }
        ImageTypeToViewTypeMap.put(DEFAULT, ViewType.DEFAULT.toString());
        ImageTypeToViewTypeMap.put(LEFT, ViewType.LEFT.toString());
        ImageTypeToViewTypeMap.put(RIGHT, ViewType.RIGHT.toString());
        ImageTypeToViewTypeMap.put(TOP, ViewType.TOP.toString());
        ImageTypeToViewTypeMap.put(BOTTOM, ViewType.BOTTOM.toString());
        ImageTypeToViewTypeMap.put(FRONT, ViewType.FRONT.toString());
        ImageTypeToViewTypeMap.put(BACK, ViewType.BACK.toString());
        ImageTypeToViewTypeMap.put(SEARCH, ViewType.SEARCH.toString());

        ViewTypeToImageTypeMap.put(ViewType.DEFAULT.toString(), DEFAULT);
        ViewTypeToImageTypeMap.put(ViewType.LEFT.toString(), LEFT);
        ViewTypeToImageTypeMap.put(ViewType.RIGHT.toString(), RIGHT);
        ViewTypeToImageTypeMap.put(ViewType.TOP.toString(), TOP);
        ViewTypeToImageTypeMap.put(ViewType.BOTTOM.toString(), BOTTOM);
        ViewTypeToImageTypeMap.put(ViewType.FRONT.toString(), FRONT);
        ViewTypeToImageTypeMap.put(ViewType.BACK.toString(), BACK);
        ViewTypeToImageTypeMap.put(ViewType.SEARCH.toString(), SEARCH);
    }

    /*
     * creates a jdbc connection and returns it.
     */
    private static Connection getJDBCConnection() throws SQLException {
        if (con == null) {
            con = DriverManager.getConnection(dbURL, username, password);
        }
        return con;
    }

    /**
     * Inserts into table value true for column "migrated" for given productId
     *
     * @param productId
     */
    private static void addProductToMigrated(Long productId) {
        Connection connection = null;
        try {
            connection = getJDBCConnection();
        } catch (SQLException ex) {
            System.out.println("Error : In addProductToMigrated. while connecting to db. Exception found is : " + ex);
            return;
        }
        if (connection == null) {
            System.out.println("Error : In addProductToMigrated. while connecting to db. Null connection returned");
            return;
        }
        //Replace first deletes the old row and creates new row if a row already exists for same primary key. Insert does not recreate if primary key already present
        String sql = "Replace into productImageMigration (product_id,migrated) values (" + productId + "," + Boolean.TRUE + ")";
        Statement smt;
        try {
            smt = connection.createStatement();
            smt.executeUpdate(sql);
        } catch (SQLException ex) {
            System.out.println("Error : Exception found while inserting into productImageMigration table : " + ex);
        }
    }

    private static Boolean isProductMigrated(Long productId) throws SQLException {
        Connection connection = null;
        try {
            connection = getJDBCConnection();
        } catch (SQLException ex) {
            System.out.println("Error : In isProductMigrated. while connecting to db. Exception found is : " + ex);
            return false;
        }
        if (connection == null) {
            System.out.println("Error : In isProductMigrated. while connecting to db. Null connection returned");
            return false;
        }
        String sql = "Select * from productImageMigration where " + PRODUCT_ID + " = " + productId;
        System.out.println(sql);
        Statement smt;
        ResultSet rs1;
        try {
            smt = connection.createStatement();
            rs1 = smt.executeQuery(sql);
        } catch (SQLException ex) {
            System.out.println("Error : Exception found : " + ex);
            return null;
        }
        if (rs1 != null && rs1.next()) {
            //if product is excluded from migration, assume it is already migrated
            if(Boolean.TRUE.equals(rs1.getBoolean(IS_EXCLUDED_COLUMN))){
                return true;
            }else{
                return rs1.getBoolean(MIGRATED_COLUMN);
            }
        } else {
            return false;
        }
    }

    /**
     * executes query to fetch all styles and returns that set Returns null in
     * case of any exceptions
     *
     */
    private static ResultSet getAllProducts() {
        Connection connection;
        try {
            connection = getJDBCConnection();
        } catch (SQLException ex) {
            System.out.println("Error : while connecting to db. Exception found is : " + ex);
            return null;
        }
        if (connection == null) {
            System.out.println("Error : while connecting to db. Null connection returned");
            return null;
        }
        String sql;
        if (styleIdsCSV == null) {
            sql = "Select * from mk_style_properties where style_id >= " + styleIdFrom + " and style_id <=" + styleIdTo + " order by style_id desc";
        } else {
            sql = "Select * from mk_style_properties where style_id in (" + styleIdsCSV + ") order by style_id desc";
        }
        Statement smt;
        ResultSet rs;
        try {
            smt = connection.createStatement();
            rs = smt.executeQuery(sql);
        } catch (SQLException ex) {
            System.out.println("Error : Exception found : " + ex);
            return null;
        }
        return rs;
    }

    /**
     * removes the domain part from image
     *
     * @param url : input url from which domain needs to be removed to get the
     * path
     * @param domain
     * @return path without domain part
     */
    private static String getPathFromURL(String url, String domain) {
        if (url == null || url.isEmpty() || domain == null) {
            return url;
        }
        String path;
        path = url.replace(domain, "");
        path = path.trim();//to remove spaces at the start or end
        return path;
    }

    //returns domain of the image url
    private static String getDomainFromURL(String url) {
        if(url == null ){
            return url;
        }
        if (url.contains(DOMAIN_S3)) {
            return DOMAIN_S3;
        } else if (url.contains(DOMAIN_CL)) {
            return DOMAIN_CL;
        }
        return null;
    }

    private static ContentResponse searchContentByProductId(Long productId) throws WebClientException {
        Map<String, String> searchParams = new HashMap<String, String>();
        searchParams.put("productIds", productId.toString());
        ContentResponse response = ContentClient.search(serviceURL, searchParams, 0, 1, info);
        return response;
    }

    private static void migrateProduct(ResultSet rs) {
        imageNumMismatch = Boolean.FALSE;
        Long productId;
        try {
            productId = Long.parseLong(rs.getString(STYLE_ID));
            //1. search contentBy productId
            ContentResponse productResponse;
            try {
                productResponse = searchContentByProductId(productId);
            } catch (WebClientException ex) {
                System.out.println("Error : while searching for contentEntity by productId. " + ex);
                return;
            }
            //if no entity fetched for given productId, create productImages,productAlbum and productContentEntity
            if (productResponse == null || !productResponse.getStatus().isSuccess()
                    || productResponse.getContentEntries() == null || productResponse.getContentEntries().isEmpty()) {
                /*To Do : What if productImages or productAlbums already present but no productContentEntity formed */
                //create productImages,productAlbum and productContentEntity
                System.out.println("Creating product content entity");
                List<ContentEntry> productImagesCollection = getProductImages(rs, ImageTypeToViewTypeMap.keySet());
                if (CollectionUtils.isNotEmpty(productImagesCollection)) {
                    ContentResponse productAlbumResponse = createProductAlbumEntity(rs, productImagesCollection);
                    if (productAlbumResponse == null
                            || !productAlbumResponse.getStatus().isSuccess()
                            || productAlbumResponse.getContentEntries() == null || productAlbumResponse.getContentEntries().isEmpty()) {
                        System.out.println("Error : creating productImage collection for product id " + rs.getString("style_id"));
                        return;
                    } else {
                        //if product album is created successfully, then create product entity containing this product album
                        productResponse = createProductEntity(rs, productAlbumResponse.getContentEntries());
                        if (productResponse == null || !productResponse.getStatus().isSuccess()//|| !productResponse.getStatus().getStatusType().toString().equals(StatusResponse.Type.SUCCESS.toString())
                                || productResponse.getContentEntries() == null || productResponse.getContentEntries().isEmpty()) {
                            System.out.println("Error : creating product from product album for product id " + rs.getString(STYLE_ID));
                            return;
                        } else {
                            System.out.println("Success : Successfully created product content entity for product id " + rs.getString(STYLE_ID));
                            //addProductToMigrated(productId);
                        }
                    }
                } else {
                    System.out.println("No images found for StyleId : " + rs.getString(STYLE_ID));
                    return;
                }
            } //if entity for given productId already present, check deeper
            else {
                System.out.println("Searched by productId successfull");
                /* TO DO : check that value of productResponse.getContentEntries().size() should be one as productId can be associated only with one productContentEntity */
                ContentEntry productContentEntry = productResponse.getContentEntries().get(0);
                //check if it has productAlbum or not
                if (CollectionUtils.isEmpty(productContentEntry.getProductAlbumEntries())) {
                    System.out.println("But album not present");
                    //if productAlbum not present, create it and associate it with productContentEntry
                    //create productImages,productAlbum and update productContentEntity
                    List<ContentEntry> productImagesCollection = getProductImages(rs, ImageTypeToViewTypeMap.keySet());
                    if (CollectionUtils.isNotEmpty(productImagesCollection)) {
                        ContentResponse productAlbumResponse = createProductAlbumEntity(rs, productImagesCollection);
                        if (productAlbumResponse == null
                                || !productAlbumResponse.getStatus().isSuccess()
                                //|| !productAlbumResponse.getStatus().getStatusType().toString().equals(StatusResponse.Type.SUCCESS.toString())
                                || productAlbumResponse.getContentEntries() == null || productAlbumResponse.getContentEntries().isEmpty()) {
                            System.out.println("Error : creating productImage collection for product id " + rs.getString("style_id"));
                            return;
                        } else {
                            //if product album is created successfully, then update product entity by adding product album to it
                            //productResponse = createProductEntity(rs, productAlbumResponse.getContentEntries());
                            productResponse = updateProductEntity(productContentEntry, productAlbumResponse.getContentEntries());
                            if (productResponse == null || !productResponse.getStatus().isSuccess() //!productResponse.getStatus().getStatusType().toString().equals(StatusResponse.Type.SUCCESS.toString())
                                    || productResponse.getContentEntries() == null || productResponse.getContentEntries().isEmpty()) {
                                System.out.println("Error : creating product from product album for product id " + rs.getString(STYLE_ID));
                                return;
                            } else {
                                System.out.println("Success : Successfully updated product content entity for product id " + rs.getString(STYLE_ID));
                                //addProductToMigrated(productId);
                            }
                        }
                    } else {
                        System.out.println("No images found for StyleId : " + rs.getString(STYLE_ID));
                    }
                } //if product album already present, go deeper
                else {
                    System.out.println("Album already present");
                    //get productAlbum content entity using productAlbumContentId present in productAlbum
                    ProductAlbumEntry albumEntry = productContentEntry.getProductAlbumEntries().get(0);
                    Long productAlbumContentId = albumEntry.getProductAlbumId();
                    ContentResponse productAlbumResponse;
                    try {
                        //findById for this productAlbumContentEntity and check for its collections
                        productAlbumResponse = ContentClient.findById(serviceURL, productAlbumContentId, info);
                        if (productAlbumResponse == null || !productAlbumResponse.getStatus().isSuccess() // getStatusType().toString().equals(StatusResponse.Type.SUCCESS.toString())
                                || productAlbumResponse.getContentEntries() == null || productAlbumResponse.getContentEntries().isEmpty()) {
                            System.out.println("Error : No result found for productAlbumContentEntity for id " + productAlbumContentId);
                            return;
                        } else {
                            ContentEntry productAlbumContentEntry = productAlbumResponse.getContentEntries().get(0);
                            //if productImage collection not present then create it
                            if (CollectionUtils.isEmpty(productAlbumContentEntry.getCollectionMembers())) {
                                System.out.println("productImage collection not present in productAlbum");
                                List<ContentEntry> productImagesCollection = getProductImages(rs, ImageTypeToViewTypeMap.keySet());
                                if (CollectionUtils.isNotEmpty(productImagesCollection)) {
                                    productAlbumResponse = updateProductAlbumEntity(productAlbumContentEntry, productImagesCollection);//createProductAlbumEntity(rs, productImagesCollection);
                                    if (productAlbumResponse == null || !productAlbumResponse.getStatus().isSuccess() //getStatusType().toString().equals(StatusResponse.Type.SUCCESS.toString())
                                            || productAlbumResponse.getContentEntries() == null || productAlbumResponse.getContentEntries().isEmpty()) {
                                        System.out.println("Error : creating productAlbum from productImages for product id " + rs.getString(STYLE_ID));
                                        return;
                                    } else {
                                        System.out.println("Success : Successfully update product album content entity for product id " + rs.getString(STYLE_ID));
                                        //addProductToMigrated(productId);
                                    }
                                }
                            } else {//if productImage collection already present, then go deeper
                                System.out.println("productImage collection is present in productAlbum");
                                //now for all productImages which are not present in this collection, create a list of viewTypes
                                Set<String> imageTypeRemaining = processCollectionForAbsentViewTypes(productAlbumContentEntry.getCollectionMembers(), rs);//ImageTypeToViewTypeMap.keySet();
                                System.out.println("ImageType which is not mapped to productAlbums are : " + imageTypeRemaining.toString());
                                if (CollectionUtils.isNotEmpty(imageTypeRemaining)) {
                                    List<ContentEntry> productImagesCollection = getProductImages(rs, imageTypeRemaining);
                                    if (CollectionUtils.isNotEmpty(productImagesCollection)) {
                                        productAlbumResponse = updateProductAlbumEntity(productAlbumContentEntry, productImagesCollection);//createProductAlbumEntity(rs, productImagesCollection);
                                        if (productAlbumResponse == null || !productAlbumResponse.getStatus().isSuccess()// getStatusType().toString().equals(StatusResponse.Type.SUCCESS.toString())
                                                || productAlbumResponse.getContentEntries() == null || productAlbumResponse.getContentEntries().isEmpty()) {
                                            System.out.println("Error : creating productAlbum from productImages for product id " + rs.getString(STYLE_ID));
                                            return;
                                        } else {
                                            System.out.println("Success : Successfully update product album content entity for product id " + rs.getString(STYLE_ID));
                                            //addProductToMigrated(productId);
                                        }
                                    }
                                }
                            }
                        }
                    } catch (WebClientException ex) {
                        System.out.println("Error : on finding productAlbumContentEntity by id : " + ex);
                    }
                }
            }
            //if some common operations are performed here at the end, then in case of error caught above, return statement should be present to avoid execution of this common code
            //if mismatch is found in num of images from cms and portal, then do not set product to migrated
            if(!imageNumMismatch){
                addProductToMigrated(productId);
            }else{
                System.out.println("Error : StyleId : "+productId+" : mismatch in number of images in cms and portal");
            }
            if (!findProduct(productId)) {
                createProduct(productId);
            }

        } catch (SQLException ex) {
            System.out.println("Error : Exception fetching product id from result set");
        }
    }

    /**
     * Call catalogClient to find if product already present for given product
     * id
     *
     * @param productId
     * @return
     */
    private static Boolean findProduct(Long productId) {
        try {
            ProductResponse response = CatalogClient.findById(serviceURL, productId, null, info);
            if (response != null && CollectionUtils.isNotEmpty(response.getData())) {
                if (CollectionUtils.isNotEmpty(response.getData())) {
                    if (CollectionUtils.isNotEmpty(response.getData().get(0).getImageCollection())) {
                        if (CollectionUtils.isNotEmpty(response.getData().get(0).getImageCollection().get(0).getImageEntries())) {
                            List<ProductImageEntry> images = response.getData().get(0).getImageCollection().get(0).getImageEntries();
                            for (ProductImageEntry eachImage : images) {
                                System.out.println("Image entries are : " + eachImage.getViewType() + " : " + eachImage.getDomain() + eachImage.getImagePath());
                            }

                        }
                    }
                }
                return true;
            }
            return false;
        } catch (WebClientException e) {
            System.out.println("Error");
            return false;
        }
    }

    private static void createProduct(Long productId) {
        try {
            ProductContentEntry entry = new ProductContentEntry();
            entry.setId(productId);
            ProductResponse response = CatalogClient.create(serviceURL, entry, info);
        } catch (WebClientException ex) {
            System.out.println("Error occured while creating product entity. Error is : " + ex);
        }
    }

    /**
     * This function returns set of viewTypes for which content entities are not
     * present in given collection
     *
     * @param collectionMemberEntries : list containing album content entity id
     * and its associated product image content entity
     * @return set of viewTypes for which product image content entities not
     * present in this collection
     */
    private static Set<String> processCollectionForAbsentViewTypes(List<CollectionMemberEntry> collectionMemberEntries, ResultSet rs) {
        Set<String> imageTypeRemaining = new HashSet<String>();
        for (String imgType : ImageTypeToViewTypeMap.keySet()) {
            imageTypeRemaining.add(imgType);
        }
        if (CollectionUtils.isEmpty(collectionMemberEntries)) {
            return imageTypeRemaining;
        }
        //iterate over all memebers to fetch member id and then find contententity for that memeber id and process it to get its viewType
        for (CollectionMemberEntry memberEntry : collectionMemberEntries) {
            Long collectionMemberId = memberEntry.getCollectionMemberId();
            try {
                ContentResponse response = ContentClient.findById(serviceURL, collectionMemberId, info);
                if (response != null && response.getStatus().isSuccess() //getStatusType().toString().equals(StatusResponse.Type.SUCCESS.toString())
                        && response.getContentEntries() != null && !response.getContentEntries().isEmpty()) {
                    ContentEntry collectionMember = response.getContentEntries().get(0);
                    if (collectionMember != null) {
                        Map<String, String> attributes = collectionMember.getAttributes();
                        String viewType = attributes.get(EntityAttributeEnum.VIEW_TYPE.toString());
                        //check if its asset has same image path than that in old schema or not. Log it
                        if (collectionMember.getAssets() != null) {
                            String path = collectionMember.getAssets().get(0).getPath();
                            String oldSchemaURL = rs.getString(ViewTypeToImageTypeMap.get(viewType));
                            //if url doesn't
                            String oldDomain = getDomainFromURL(oldSchemaURL);
                            String oldPath = getPathFromURL(oldSchemaURL, oldDomain);
                            if (oldPath == null || !oldPath.equals(path)) {
                                System.out.println("For view Type " + viewType + " path in old schema and new schema doesn't match. [Old schema Path, new schema path] : [" + oldPath + " , " + path + "]");
                            }
                            imageTypeRemaining.remove(ViewTypeToImageTypeMap.get(viewType));
                        }
                    }
                } else {
                    System.out.println("Error : In processCollectionForAbsentViewTypes : Content entity for productImage content entity with id " + collectionMemberId + " not found.");
                }
            } catch (WebClientException ex) {
                System.out.println("Error : In function processCollectionForAbsentViewTypes, exception found : " + ex);
            } catch (SQLException ex) {
                System.out.println("Error : In function processCollectionForAbsentViewTypes, SQL Exception found : " + ex);
            }
        }
        return imageTypeRemaining;
    }

    /**
     * main function
     */
    public static void main(String[] args) {
        System.out.println(args.length);
        if (args.length < 5) {
            System.out.println("Assuming command line to be in the form");
            System.out.println("java  -cp \"classpath\" ProductImagesMigrationScript dbusername dbpassword dburl cmsserviceUrl styleIdFrom styleIdTo");
            System.out.println("OR");
            System.out.println("java  -cp \"classpath\" ProductImagesMigrationScript dbusername dbpassword dburl cmsserviceUrl absolutePathofTheFileContainingStyleIdCSV");
            return;
        }

        /*Assuming command line to be in the form
         * java -cp "classpath" ProductImagesMigrationScript username password dburl cmsserviceUrl styleIdFrom styleIdTo  or
         * java -cp "classpath" ProductImagesMigrationScript username password dburl cmsserviceUrl absolutePathofTheFileContainingStyleIdCSV
         */
        username = args[0];
        password = args[1];
        dbURL = args[2];
        serviceURL = args[3];
        if (args.length == 6) {
            try {
                styleIdFrom = Long.parseLong(args[4]);
            } catch (NumberFormatException ex) {
                System.out.println("StyleIdFrom should be of type Long");
                return;
            }
            try {
                styleIdTo = Long.parseLong(args[5]);
            } catch (NumberFormatException ex) {
                System.out.println("StyleIdTo should be of type Long");
                return;
            }
        } else {
            String fileName = args[4];
            try {
                BufferedReader reader = new BufferedReader(new FileReader(fileName));
                String line = reader.readLine();
                if (line != null) {
                    styleIdsCSV = line;
                } else {
                    System.out.println("No data read from the file containing style ids csv");
                    return;
                }

            } catch (IOException e) {
                System.out.println("Error while reading file containing style ids csv");
                return;
            }
        }

        System.out.println("Input received : ");
        if (args.length == 6) {
            System.out.println("dbusername " + username + "\ndbpassword " + password + "\ndburl " + dbURL + "\ncmsserviceUrl " + serviceURL
                    + "\nstyleIdFrom " + styleIdFrom + "\nstyleIdTo " + styleIdTo);
        } else {
            System.out.println("dbusername " + username + "\ndbpassword " + password + "\ndburl " + dbURL + "\ncmsserviceUrl " + serviceURL
                    + "\nFileName " + args[4]);
        }

        //Read database to get style entity
        initialize();
        //Set parameters for using threads
        initializeThreads();

        ResultSet rs = getAllProducts();
        if (rs != null) {
            try {
                while (rs.next()) {
                    Long productId = Long.parseLong(rs.getString(STYLE_ID));
                    if (!isProductMigrated(productId)) {
                        System.out.println("product id " + productId + " is not yet migrated");
                        migrateProduct(rs);
                    } else {
                        System.out.println("Success : product id " + productId + " is already migrated or is excluded from migration");
                    }
                }
            } catch (SQLException ex) {
                System.out.println("Error : occurred while reading result set. Exception is : " + ex);
            }
        } else {
            System.out.println("Error : Null result set found");
        }
        System.out.println("Finishing threads..");
        finishThreads();
        System.out.println("Done");

    }

    private static ContentResponse updateProductAlbumEntity(ContentEntry productAlbumEntry, List<ContentEntry> productImagesCollection) {
        List<CollectionMemberEntry> oldCollection = productAlbumEntry.getCollectionMembers();
        if (CollectionUtils.isEmpty(productImagesCollection)) {
            return null;
        }

        List<CollectionMemberEntry> collectionEntryList = new ArrayList<CollectionMemberEntry>();
        for (ContentEntry productImage : productImagesCollection) {
            collectionEntryList.add(convertContentEntryToCollectionEntry(productImage));
        }
        if (CollectionUtils.isNotEmpty(oldCollection)) {
            collectionEntryList.addAll(oldCollection);
        }
        productAlbumEntry.setCollectionMembers(collectionEntryList);

        try {
            ContentResponse response = ContentClient.update(serviceURL, productAlbumEntry.getId(), productAlbumEntry, info);
            return response;
        } catch (WebClientException ex) {
            System.out.println("Error : while updating album content entity ");
            return null;
        }
    }

    private static ContentResponse updateProductEntity(ContentEntry productContentEntry, List<ContentEntry> albumContentEntries) {
        if (CollectionUtils.isEmpty(albumContentEntries)) {
            return null;
        }
        List<ProductAlbumEntry> productAlbumList = new ArrayList(albumContentEntries.size());
        for (ContentEntry contentEntry : albumContentEntries) {
            ProductAlbumEntry albumEntry = new ProductAlbumEntry();
            albumEntry.setProductAlbumId(contentEntry.getId());
            productAlbumList.add(albumEntry);
        }
        List<ProductAlbumEntry> oldAlbumEntries = productContentEntry.getProductAlbumEntries();
        if (CollectionUtils.isNotEmpty(oldAlbumEntries)) {
            productAlbumList.addAll(oldAlbumEntries);
        }
        productContentEntry.setProductAlbumEntries(productAlbumList);
        ContentResponse response = null;
        try {
            response = ContentClient.update(serviceURL, productContentEntry.getId(), productContentEntry, info);
        } catch (WebClientException ex) {
            System.out.println("Error : while updating productContentEntry with productId " + productContentEntry.getProductId() + " for productAlbum : " + ex);
        }
        return response;

    }

    /**
     *
     * @param rs : result set containing details about a product
     * @param albumContentEntries : list of product albums
     * @return response containing newly created product content entry
     * containing product albums
     */
    private static ContentResponse createProductEntity(ResultSet rs, List<ContentEntry> albumContentEntries) {
        try {
            ContentEntry productEntry = new ContentEntry();
            Long styleId = rs.getLong(STYLE_ID);
            productEntry.setProductId(styleId);
            productEntry.setType(PRODUCT);
            List<ProductAlbumEntry> productAlbumList = new ArrayList(albumContentEntries.size());
            for (ContentEntry contentEntry : albumContentEntries) {
                ProductAlbumEntry albumEntry = new ProductAlbumEntry();
                albumEntry.setProductAlbumId(contentEntry.getId());
                productAlbumList.add(albumEntry);
            }
            productEntry.setProductAlbumEntries(productAlbumList);
            ContentResponse response = ContentClient.create(serviceURL, productEntry, info);
            return response;
        } catch (SQLException ex) {
            System.out.println("Error : Exception caught while reading style id from result set. Exception is " + ex);
            return null;
        } catch (WebClientException ex) {
            System.out.println("Error : Exception while creating content entity for product. Exception is : " + ex);
            return null;
        }
    }

    /**
     * This function creates wrapping content entity for a product and adds list
     * of product images associated with it
     *
     * @param rs : result set containing product related information
     * @param productImagesCollection : list of product image contentEntries for
     * images corresponding to product in result set
     * @return response of creating product content entity
     */
    private static ContentResponse createProductAlbumEntity(ResultSet rs, List<ContentEntry> productImagesCollection) {
        try {
            ContentEntry productContentEntry = new ContentEntry();
            //productContentEntry.setProductId(rs.getLong("style_id"));
            productContentEntry.setType(PRODUCT_ALBUM_TYPE);
            Map<String, String> albumAttributes = new HashMap<String, String>();
            //setting property of default album
            albumAttributes.put(EntityAttributeEnum.IS_DEFAULT.toString(), "true");
            albumAttributes.put(EntityAttributeEnum.SORT_ORDER.toString(), "1");
            albumAttributes.put(EntityAttributeEnum.NAME.toString(), "Default");
            productContentEntry.setAttributes(albumAttributes);
            if (productImagesCollection != null) {
                List<CollectionMemberEntry> collectionEntryList = new ArrayList<CollectionMemberEntry>(productImagesCollection.size());
                for (ContentEntry productImage : productImagesCollection) {
                    collectionEntryList.add(convertContentEntryToCollectionEntry(productImage));
                }
                productContentEntry.setCollectionMembers(collectionEntryList);
            }
            try {
                ContentResponse response = ContentClient.create(serviceURL, productContentEntry, info);
                return response;
            } catch (WebClientException ex) {
                System.out.println("Error : while creating content entity for product id " + rs.getLong(STYLE_ID));
                return null;
            }
        } catch (SQLException ex) {
            System.out.println("Error : while reading style id from result set");
            return null;
        }

    }

    /**
     * This function creates collection entry from content entry
     *
     * @param contentEntry contentEntry for which collection entry needs to be
     * created
     * @return collection Entry containing collection member id as that of input
     * contentEntry id
     */
    private static CollectionMemberEntry convertContentEntryToCollectionEntry(ContentEntry contentEntry) {
        CollectionMemberEntry collectionEntry = new CollectionMemberEntry();
        collectionEntry.setCollectionMemberId(contentEntry.getId());
        return collectionEntry;
    }

    /**
     * Converts column name in mk_style_properties table to viewType enum
     *
     * @param imageView : column name of the image
     * @return viewType value corresponding to given image name
     */
    private static String getViewTypeFromImage(String imageView) {
        if (MapUtils.isEmpty(ImageTypeToViewTypeMap)) {
            fillImageToViewMap();
        }
        return ImageTypeToViewTypeMap.get(imageView);
    }

    /**
     * This function creates wrapping content entity for each image
     * corresponding to the product passed in input
     *
     * @param rs : result set containing product related information of which
     * product images is to be created
     * @param viewTypes : view type of the image for which productImage content
     * entity is to be created
     * @return List of product image content entries for all images of this
     * product
     */
    private static List<ContentEntry> getProductImages(ResultSet rs, Set<String> imageTypes) {
        List<ContentEntry> productImageList = new ArrayList<ContentEntry>();
        int numPortalImages=0;
        try {
            Long productId = Long.parseLong(rs.getString("style_id"));

            if (CollectionUtils.isNotEmpty(imageTypes)) {
                String searchImageURL = "";
                int numThreadsCreated = 0;
                List<Future<ContentResponse>> submittedFutures = new ArrayList<Future<ContentResponse>>();
                
                for (String imageType : imageTypes) {
                    String imageURL;
                    //if imageType is of search, get url from default image, store it in variable and migrate it after all images are upload (since it depends upon completion of default image migration)
                    if (imageType.equals(SEARCH)) {
                        searchImageURL = rs.getString(DEFAULT);
                        continue;
                    } else {
                        imageURL = rs.getString(imageType);
                    }

                    //creating content entry for the image
                    if (imageURL != null && !imageURL.isEmpty()) {
                        String viewType = getViewTypeFromImage(imageType);
                        System.out.println("Submitting request for viewType " + viewType + " at time : " + System.currentTimeMillis());
                        Future<ContentResponse> future = completionService.submit(new productImageCreationTask(imageURL, viewType));
                        submittedFutures.add(future);
                        numThreadsCreated++;
                        numPortalImages++;
                    }
                }
                
                //now take result from threads as they complete
                for (int i = 0; i < numThreadsCreated; i++) {
                    List<ContentEntry> entries = getThreadResponse();
                    if(CollectionUtils.isNotEmpty(entries)){
                        productImageList.addAll(entries);
                    }else{
                        System.out.println("No response found for product id " + productId + " and view type NotKnown");
                    }
                }
                
                //Now migrate search image (this is not done in parallel with other images because it depends upon default image whose asset is used by search entity
                if (StringUtils.isNotEmpty(searchImageURL)) {
                    System.out.println("Submitting request for viewType " + ViewType.SEARCH.toString()+ " at time : " + System.currentTimeMillis());
                    Future<ContentResponse> future = completionService.submit(new productImageCreationTask(searchImageURL, ViewType.SEARCH.toString()));
                    List<ContentEntry> entries = getThreadResponse();
                    if(CollectionUtils.isNotEmpty(entries)){
                        productImageList.addAll(entries);
                    }else{
                        System.out.println("No response found for product id " + productId + " and view type Search");
                    }
                    numPortalImages++;
                }
            }

        } catch (SQLException ex) {
            System.out.println("Error : Sql exception found : " + ex);
        }
        if(productImageList.size() != numPortalImages){
            imageNumMismatch = Boolean.TRUE;
        }
        return productImageList;
    }

    /**
     * Waits on the result from the thread to be available and extract response from it and returns
     * Returns null if any error occurs
     * @return 
     */
    private static List<ContentEntry> getThreadResponse() {
        Future<ContentResponse> future = null;
        try {
            future = completionService.poll(productImageCreationTimeOut, TimeUnit.MINUTES);
            if (future != null) {
                ContentResponse response = future.get();
                System.out.println("Got response at time : " + System.currentTimeMillis());
                //ContentResponse response = createContentEntry(imageURL,null,viewType);
                if (response == null || !response.getStatus().isSuccess() // !response.getStatus().getStatusType().equals(StatusResponse.Type.SUCCESS.toString())
                        || CollectionUtils.isEmpty(response.getContentEntries())) {
                } else {
                    return response.getContentEntries();
                }
            } else {
                System.out.println("Time out.. task not completed");
            }
        } catch (InterruptedException ex) {
            System.out.println("Error : InterruptedException in threads : " + ex);
        } catch (ExecutionException ex) {
            System.out.println("Error : ExecutionException in threads : " + ex);
        } finally {//Cancelling any remaining 
            if (future != null) {
                future.cancel(true);
            }
        }
        return null;
    }
    /*
     * This adds default resoluions in a map and returns it
     */
    public static HashMap<String, AssetResolution> getDefaultResolutions() {
        HashMap<String, AssetResolution> resolutionMap = new HashMap<String, AssetResolution>();
//        1080_1440.jpg 150_200.jpg 180_240.jpg 360_480.jpg 48_64.jpg
        resolutionMap.put("1080X1440", null);
        resolutionMap.put("150X200", null);
        resolutionMap.put("180X240", null);
        resolutionMap.put("360X480", null);
        resolutionMap.put("48X64", null);
        resolutionMap.put("81X108", null);
        resolutionMap.put("540X720", null);
        resolutionMap.put("96X128", null);
        return resolutionMap;
    }

    /*
     * This adds resoluions in a map for all view type other than default and returns it
     */
    public static HashMap<String, AssetResolution> getOtherResolutions() {
        HashMap<String, AssetResolution> resolutionMap = new HashMap<String, AssetResolution>();
//        1080_1440.jpg 150_200.jpg 180_240.jpg 360_480.jpg 48_64.jpg
        resolutionMap.put("1080X1440", null);
        resolutionMap.put("540X720", null);
        resolutionMap.put("81X108", null);
        resolutionMap.put("360X480", null);
        resolutionMap.put("48X64", null);

        return resolutionMap;
    }

    private static class productImageCreationTask implements Callable<ContentResponse> {

        String image;
        String viewType;

        public productImageCreationTask(String image, String viewType) {
            this.image = image;
            this.viewType = viewType;
        }

        @Override
        public ContentResponse call() {
            return this.createContentEntry(image, null, viewType);
        }

        /**
         * Creates content entity from the image
         *
         * @param image : input image of which content entity is to be created
         * @param productId : product id of which this image is a part of
         * @param viewType : view type of the image (front,back,top etc)
         * @return response returned after calling content client create
         * function
         */
        private ContentResponse createContentEntry(String image, Long productId, String viewType) {
            //create assetEntry and put it int contentEntry
            AssetEntry assetEntry = new AssetEntry();
            String domain = getDomainFromURL(image);
            String path = getPathFromURL(image, domain);

            assetEntry.setDomain(domain);
            assetEntry.setPath(path);
            assetEntry.setFormat("jpg");
            //If viewType is default or search (search image url will be same as default) use one set of resolutions, else use other set
            if (viewType.equals(ViewType.DEFAULT.toString()) || viewType.equals(ViewType.SEARCH.toString())) {
                assetEntry.setResolutions(getDefaultResolutions());
            } else {
                assetEntry.setResolutions(getOtherResolutions());
            }

            List<AssetEntry> assetEntries = new ArrayList();
            assetEntries.add(assetEntry);
            ContentEntry contentEntry = new ContentEntry();
            contentEntry.setType(PRODUCT_IMAGE);
            contentEntry.setAssets(assetEntries);
            contentEntry.setProductId(productId);
            Map<String, String> entityAttributes = new HashMap<String, String>();
            entityAttributes.put(EntityAttributeEnum.VIEW_TYPE.toString(), viewType);
            contentEntry.setAttributes(entityAttributes);
            ContentResponse bannerResponse;
            try {
                bannerResponse = ContentClient.create(serviceURL, contentEntry, info);
                return bannerResponse;
            } catch (WebClientException ex) {
                System.out.println("For product id " + productId + ".Not able to create content for image with path : "
                        + assetEntry.getDomain() + assetEntry.getPath() + "  & view type " + viewType.toString());
                return null;
            }
        }
    };
}
