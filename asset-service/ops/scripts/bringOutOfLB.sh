#!/bin/sh

source /myntra/myntra-cms-service/releases/current/bin/setenv.sh

echo "Server root is $SERVER_ROOT"

sed -i 's/healthcheck.status=1/healthcheck.status=0/g' $SERVER_ROOT/conf/application.properties

echo "Brought Out of LB"
