#!/bin/sh
source /myntra/myntra-cms-service/releases/current/bin/setenv.sh
if [ -e "$CATALINA_PID" ]
then
    cat "$CATALINA_PID";
else
    # this should not be called - because we are setting and touching the pid file
    ps -eaf | grep tomcat | grep "myntra-cms-service" | grep -v grep |  awk '{print $2}'
fi
