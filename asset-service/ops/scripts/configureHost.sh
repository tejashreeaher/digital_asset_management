#!/bin/sh

SRC_DIR=$1
TARGET_DIR=$2
echo "Copying from OPS_DIR Located at $SRC_DIR"
echo "Copying to   APP_DIR Located at $TARGET_DIR"
mv $SRC_DIR/setenv.sh $TARGET_DIR/bin
echo "moving $SRC_DIR/setenv.sh to $TARGET_DIR/bin"

cp -a $SRC_DIR/*  $TARGET_DIR/conf
chmod -R +x $TARGET_DIR/bin
mkdir -p $TARGET_DIR/logs
mkdir -p $TARGET_DIR/temp

echo "Configured"
