/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.myntra.database.entity;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
/**
 *
 * @author tejashree.aher
 */
@Entity
@Table(name = "employee")
@NamedQueries({
    @NamedQuery(name = "getAllEmployees", query = "SELECT s FROM Employee s"),
    @NamedQuery(name = "getById", query = "SELECT s FROM Employee s where empId = (:employeeid)")
})
public class Employee {
    @Id
    @Column(name = "id")
    private Long empId;
    
    @Column(name = "name")
    private String empName;
    
}
